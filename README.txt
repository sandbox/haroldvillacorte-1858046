********************************************************************************
PHPMailer Contact

This module is intended to be an alternative to the core Contact module. It is
recommended for those who have been unsuccessful in getting the Drupal 7 Contact
module to send messages and other contributed modules have not worked to their
satisfaction.  Many who are on shared hosting have experienced this as well
as those who are having trouble configuring up their own servers correctly.

This module uses the PHPMailer library, a rock solid object oriented program
that makes sending smtp emails very simple.  Users are allowed to store
multiple smtp contacts each with their own settings and passwords.  Passwords
are stored in the encrypted in the database.  A block is included which can
optionally be enabled.

********************************************************************************
Setup

1. Download the PHPMailer library at:
	http://code.google.com/a/apache-extras.org/p/phpmailer/.

2. Install the PHPMailer library so that class.smtp.php can be found at
   sites/all/libraries/phpmailer/class.smtp.php

3. Install PHPMailer Contact (this module) unpacking it to your Drupal
   sites/all/modules directory if you are isntalling by hand then enable it.

4. Set the permissions for PHPMailer Contact.

5. Rebuild access permissions if you are prompted to.

6. Go to the settings page at admin/config/people/phpmailer_contact.

7. Set your encryption key.  There is already a default key in the database
   but it is highly recommended that you set your own.  Simply enter 32
   alphanumeric upper and lowercase characters and click the Set Key button.

8. Set the contact settings according to your email server settings.

9. The Contact page is at yourdomain/phpmailer_contact.  To change this url use
	the core Path module.

10. If a contact block is desired enable the block PHPMailer Contact.
********************************************************************************
