<?php
/**
 * @file
 * Help file for the PHPMailer Contact module.
 */

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param string $path
 *   $path Which path of the site we're using to display help
 * @param string $arg
 *   $arg Array that holds the current path as returned from arg() function
 */
function phpmailer_contact_help($path, $arg) {
  $download = l(t('THIS LINK'), 'http://code.google.com/a/apache-extras.org/p/phpmailer/');
  switch ($path) {
    case 'admin/help#phpmailer_contact':
      $output  = '<p>' . t('This module is intended to be an alternative to the core Contact module. It is recommended for those') . '&nbsp';
      $output .= t('who have been unsuccessful in getting the Drupal 7 Contact module to send messages and other contributed') . '&nbsp';
      $output .= t('modules have not worked to their satisfaction.  Many who are on shared hosting have experienced this as well') . '&nbsp';
      $output .= t('as those who are having trouble configuring up their own servers correctly.') . '</p>';
      $output .= '<p>' . t('This module uses the PHPMailer library, a rock solid object oriented program that makes sending smtp emails') . '&nbsp';
      $output .= t('very simple.  Users are allowed to store multiple smtp contacts each with their own settings and passwords.') . '&nbsp';
      $output .= t('Passwords are stored in the database using 128bit encrytion.  A block is included which can be optionally enabled.') . '</p>';
      $output .= '<h5>' . t('Setup') . '</h5>';
      $output .= '<ol>';
      $output .= '<li>' . t('Download the PHPMailer library at&nbsp') . $download . '</li>';
      $output .= '<li>' . t('Install the PHPMailer library so that class.smtp.php can be found at sites/all/libraries/phpmailer/class.smtp.php') . '</li>';
      $output .= '<li>' . t('Install PHPMailer Contact (this module) unpacking it to your Drupal sites/all/modules directory if you are isntalling by hand then enable it.') . '</li>';
      $output .= '<li>' . t('Set the permissions for PHPMAiler Contact.') . '</li>';
      $output .= '<li>' . t('Rebuild access permissions if you are prompted to.') . '</li>';
      $output .= '<li>' . t('Go to the settings page at admin/config/people/phpmailer_contact.') . '</li>';
      $output .= '<li>' . t('Set your encryption key.  There is already a default key in the database but it is highly recommended that you set your own.') . '<br/>';
      $output .= t('Simply enter 32 alphanumeric upper and lowercase characters and click the Set Key button.') . '</li>';
      $output .= '<li>' . t('Set the contact settings according to your email server settings.') . '</li>';
      $output .= '<li>' . t('The Contact page is at yourdomain/phpmailer_contact.  To change this url use the core Path module.') . '</li>';
      $output .= '<li>' . t('If a contact block is desired enable the block PHPMailer Contact.') . '</li>';
      $output .= '</ol>';

      return $output;
      break;
  }
}
