<?php
/**
 * @file
 * Forms for the PHPMailer Contact module.
 */

/**
 * Form constructor for the Set Key form.
 *
 * @see phpmailer_contact_menu()
 * @see phpmailer_contact_set_key_form_validate()
 * @see phpmailer_contact_set_key_form_submit()
 *
 * @ingroup forms
 */
function phpmailer_contact_set_key_form($form, &$form_state) {

  $form['intro'] = array(
    '#markup' => t('Set your encryption key.'),
  );
  $form['phpmailer_contact_key_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#required' => FALSE,
    '#description' => t('32 alphnumeric upper and lower case characters.  Required for encrypting your password for storage in the database.') . '<br/>' . t('You can leave it alone if your smtp server does not require authentication. If you reset the key also reset the password.  Only one key is needed.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set key'),
  );
  return $form;
}

/**
 * Form validation handler for phpmailer_contact_set_key_form().
 *
 * @see phpmailer_contact_set_key_form_submit()
 */
function phpmailer_contact_set_key_form_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['phpmailer_contact_key_value']) != 32) {
    form_set_error('phpmailer_contact_key', t('Key must be 32 characters.'));
  }
  if (!ctype_alnum($form_state['values']['phpmailer_contact_key_value'])) {
    form_set_error('phpmailer_contact_key', t('Key must all alphanumeric characters.'));
  }
}

/**
 * Form submit handler for phpmailer_contact_set_key_form().
 *
 * @see phpmailer_contact_set_key_form_validate()
 */
function phpmailer_contact_set_key_form_submit($form, &$form_state) {
  phpmailer_contact_set_key($form_state['values']);
}

/**
 * Form constructor for the Choose Contact form.
 *
 * @see phpmailer_contact_menu()
 * @see phpmailer_contact_get_contacts_form_submit()
 *
 * @ingroup forms
 */
function phpmailer_contact_get_contacts_form($form, &$form_state) {

  if (isset($_SESSION['phpmailer_contact_cid'])) {
    $cid = $_SESSION['phpmailer_contact_cid'];
  }
  else {
    $cid = 'new_contact';
  }
  if ($result = phpmailer_contact_get_all_contacts()) {
    if ($result->rowCount() == 0) {
      $description = t('Contacts are empty.');
      $options = NULL;
    }
    else {
      $description = NULL;
      $options = $result->fetchAllKeyed(0, 1);
      $options['new_contact'] = '<em><strong>' . t('Add new contact') . '</strong></em>';
    }
  }

  $form['contact'] = array(
    '#type' => 'radios',
    '#title' => '<h5>' . t('Pick a contact to edit or add a new one') . '</h5>',
    '#default_value' => $cid,
    '#options' => $options,
    '#description' => t($description),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit contact'),
  );
  return $form;
}

/**
 * Form submit handler for phpmailer_contact_get_contacts_form().
 */
function phpmailer_contact_get_contacts_form_submit($form, &$form_state) {
  if ($form_state['values']['contact'] == 0) {
    unset($_SESSION['phpmailer_contact_cid']);
  }
  else {
    $_SESSION['phpmailer_contact_cid'] = $form_state['values']['contact'];
  }
}

/**
 * Form constructor for the Choose Settings form.
 *
 * @see phpmailer_contact_menu()
 * @see phpmailer_contact_settings_form_validate()
 * @see phpmailer_contact_settings_form_submit()
 *
 * @ingroup forms
 */
function phpmailer_contact_settings_form($form, &$form_state) {

  if (isset($_SESSION['phpmailer_contact_cid'])) {
    $cid = $_SESSION['phpmailer_contact_cid'];
    $contacts = phpmailer_contact_get_contact_by_cid($cid);
  }
  else {
    $contacts = NULL;
  }

  $cid = NULL;
  $name = NULL;
  $to = NULL;
  $host = NULL;
  $port = NULL;
  $auth = NULL;
  $protocol = NULL;
  $username = NULL;
  $required = FALSE;

  if (isset($contacts['cid'])) {
    $cid = $contacts['cid'];
    $name = $contacts['name'];
    $to = $contacts['to'];
    $host = $contacts['host'];
    $port = $contacts['port'];
    $auth = $contacts['auth'];
    $protocol = $contacts['protocol'];
    $username = $contacts['username'];
  }
  if ($contacts['auth'] == TRUE) {
    $required = TRUE;
  }

  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $cid,
  );
  $form['intro'] = array(
    '#markup' => t('Settings for PHPMailer Contact.'),
  );
  $form['phpmailer_contact_name'] = array(
    '#type' => 'textfield',
    '#title' => t('To name'),
    '#default_value' => $name,
    '#required' => TRUE,
    '#description' => t('The name of the person receiving the email.'),
    '#size' => 30,
  );
  $form['phpmailer_contact_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => $to,
    '#required' => TRUE,
    '#description' => t('The email of the person receiving the email. Will also be used as the from address.'),
    '#size' => 30,
  );
  $form['phpmailer_contact_host'] = array(
    '#type' => 'textfield',
    '#title' => t('SMTP host'),
    '#default_value' => $host,
    '#required' => TRUE,
    '#description' => t('For example: smtp.google.com.  If you are unsure check your email service documentation.'),
    '#size' => 30,
  );
  $form['phpmailer_contact_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => $port,
    '#required' => TRUE,
    '#description' => t('Usually 465 or 587 for secure connections and 25 for unsecure.  If you are unsure check your email service documentation.'),
    '#size' => 5,
  );
  $form['phpmailer_contact_auth'] = array(
    '#type' => 'checkbox',
    '#title' => t('<strong>Authentication Required?</strong>'),
    '#required' => FALSE,
    '#default_value' => $auth,
    '#description' => t('Check the box if your smtp server requires a username and password.'),
  );
  $form['phpmailer_contact_protocol'] = array(
    '#type' => 'textfield',
    '#title' => t('Security protocol'),
    '#default_value' => $protocol,
    '#required' => $required,
    '#description' => t('If you are using secure email enter the protocol.  For example: "ssl" or "tls".  Do not inclue the quotes.'),
    '#size' => 10,
  );
  $form['phpmailer_contact_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $username,
    '#required' => $required,
    '#description' => t('If your smtp server requires authentication enter you username. Many email services require the full email address as the username.'),
    '#size' => 30,
  );
  $form['phpmailer_contact_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => $required,
    '#description' => t('If your smtp server requires authentication enter you password. Password will be stored as encryted value in the database.'),
    '#size' => 30,
  );
  $form['delete'] = array(
    '#type' => 'radios',
    '#title' => '<em>' . t('Delete contact?') . '</em>',
    '#default_value' => NULL,
    '#options' => array('Delete'),
    '#description' => t('Only check this if you want to delete existing contact.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  return $form;
}

/**
 * Form validation handler for phpmailer_contact_settings_form().
 *
 * @see phpmailer_contact_settings_form_submit()
 */
function phpmailer_contact_settings_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['phpmailer_contact_to'])) {
    form_set_error('phpmailer_contact_to', t('Email address is not valid.'));
  }
  if (!is_numeric($form_state['values']['phpmailer_contact_port'])) {
    form_set_error('phpmailer_contact_port', t('Port must be a numeric value.'));
  }
}

/**
 * Form submit handler for phpmailer_contact_settings_form().
 *
 * @see phpmailer_contact_settings_form_validate()
 */
function phpmailer_contact_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['delete'])) {
    phpmailer_contact_delete($form_state['values']['cid']);
  }
  else {
    phpmailer_contact_save($form_state['values']);
  }
}

/**
 * Form constructor for the main Contact form.
 *
 * @see phpmailer_contact_menu()
 * @see phpmailer_contact_form_validate()
 * @see phpmailer_contact_form_submit()
 *
 * @ingroup forms
 */
function phpmailer_contact_form($form, &$form_state) {

  if ($result = phpmailer_contact_get_all_contacts()) {
    if ($result->rowCount() == 0) {
      $description = t('Contacts are empty.');
      $options = NULL;
      $type = 'radios';
    }
    else {
      $description = NULL;
      if ($result->rowCount() == 1) {
        $options = NULL;
        $contacts = $result->fetchAssoc();
        $description = NULL;
        $type = 'hidden';
      }
      else {
        $type = 'radios';
        $options = $result->fetchAllKeyed(0, 1);
        $description = NULL;
      }
    }
  }

  $width = array('style' => 'width:65%');

  $form['contact'] = array(
    '#type' => $type,
    '#options' => $options,
    '#description' => t($description),
  );

  // Add #value to the $form['contact'] array after type is set to 'hidden'
  if ($result->rowCount() == 1) {
    $form['contact']['#value'] = $contacts['cid'];
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#attributes' => $width,
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => TRUE,
    '#attributes' => $width,
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Your email'),
    '#required' => TRUE,
    '#attributes' => $width,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Form validation handler for phpmailer_contact_form().
 *
 * @see phpmailer_contact_form_submit()
 */
function phpmailer_contact_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('That e-mail address is not valid.'));
  }
  if (!isset($form_state['values']['contact'])) {
    form_set_error('contact', t('Please choose a contact to send to.'));
  }
}

/**
 * Form submit handler for phpmailer_contact_form().
 *
 * @see phpmailer_contact_form_validate()
 */
function phpmailer_contact_form_submit($form, &$form_state) {
  phpmailer_contact_mail_send($form_state['values']);
}
