<?php
/**
 * @file
 * Functions for the PHPMailer Contact  module.
 */

/**
 * @todo
 *   Perhaps write a module as interface for PHPMailer library and remove this.
 */
global $base_url;
$library = '<a href="http://code.google.com/a/apache-extras.org/p/phpmailer/">' . t('here') . '</a>';
$documentation = '<a href="' . $base_url . '/admin/help/phpmailer_contact">' . t('documentation') . '</a>';
$path1 = 'sites/all/libraries/phpmailer/class.phpmailer.php';
$path2 = 'sites/all/libraries/phpmailer/class.phpmailer.php';
// Check if the PHPMailer library exists.
if (file_exists($path1) && file_exists($path2)) {
  require_once 'sites/all/libraries/phpmailer/class.phpmailer.php';
  require_once 'sites/all/libraries/phpmailer/class.smtp.php';
}
else {
  drupal_set_message(t('PHPMailer Contact module: required library is not installed correctly.  Please download '
    . $library .
    ' and refer to the '
    . $documentation), 'error');
}

/**
 * Custom page callback function.
 *
 * @return array
 *   "$build" An array of form constructors.
 *
 * @see phpmailer_contact_menu()
 */
function phpmailer_contact_custom_forms_callback() {
  $build = array(
    'form_one' => drupal_get_form('phpmailer_contact_set_key_form'),
    'form_two' => drupal_get_form('phpmailer_contact_get_contacts_form'),
    'form_three' => drupal_get_form('phpmailer_contact_settings_form'),
  );
  return $build;
}

/**
 * Sets ecryption key and IV.
 *
 * @see phpmailer_contact_set_key_form()
 */
function phpmailer_contact_set_key($form_values) {
  $alg = MCRYPT_RIJNDAEL_256;
  $iv = mcrypt_create_iv(mcrypt_get_iv_size($alg, MCRYPT_MODE_ECB), MCRYPT_RAND);
  $result = db_update('phpmailer_contact_key')
    ->fields(array(
    'phpmailer_contact_key_value' => $form_values['phpmailer_contact_key_value'],
    'phpmailer_contact_iv' => $iv,
  ))
    ->execute();
  if ($result == TRUE) {
    drupal_set_message(t('Key was set succesfully.'));
  }
  else {
    drupal_set_message(t('There was a problem setting the key.', 'error'));
  }
}

/**
 * Password is passed in then returns encryted string.
 *
 * @param string $string
 *   $string User defined password.
 *
 * @return string
 *   $encrypted_string Encrypted using PHP's mcrypt_encrypt().
 *
 * @see phpmailer_contact_save()
 */
function phpmailer_contact_encrypt($string) {
  $key = phpmailer_contact_get_key();
  $alg = MCRYPT_RIJNDAEL_256;
  if ($encrypted_string = mcrypt_encrypt($alg, $key['key'], $string, MCRYPT_MODE_CBC, $key['iv'])) {
    return $encrypted_string;
  }
  else {
    drupal_set_message(t('Unable to encrypt string.'), 'error');
  }
}

/**
 * Encrypted string is passed in then returns decryted string.
 *
 * @param string $encrypted_string
 *   $encrypted_string Encrypted password is fetched from the database.
 *
 * @return string
 *   $decrypted_string Decrypted using PHP's mcrypt_decrypt().
 *
 * @see phpmailer_contact_mail_send()
 */
function phpmailer_contact_decrypt($encrypted_string) {
  $key = phpmailer_contact_get_key();
  $alg = MCRYPT_RIJNDAEL_256;
  if ($decrypted_string = mcrypt_decrypt($alg, $key['key'], $encrypted_string, MCRYPT_MODE_CBC, $key['iv'])) {
    return $decrypted_string;
  }
  else {
    drupal_set_message(t('Unable to decrypt string.'), 'error');
  }
}

/**
 * Saves contact settings to database.
 *
 * @param array $form_values
 *   $form_values User defined smtp server settings.
 *
 * @see phpmailer_contact_settings_form_submit()
 */
function phpmailer_contact_save($form_values) {
  $encrypted_password = phpmailer_contact_encrypt($form_values['phpmailer_contact_password']);
  $save_array = array(
    'phpmailer_contact_name' => $form_values['phpmailer_contact_name'],
    'phpmailer_contact_to' => $form_values['phpmailer_contact_to'],
    'phpmailer_contact_host' => $form_values['phpmailer_contact_host'],
    'phpmailer_contact_port' => $form_values['phpmailer_contact_port'],
    'phpmailer_contact_auth' => $form_values['phpmailer_contact_auth'],
    'phpmailer_contact_protocol' => $form_values['phpmailer_contact_protocol'],
    'phpmailer_contact_username' => $form_values['phpmailer_contact_username'],
    'phpmailer_contact_password' => $encrypted_password,
  );
  if (isset($form_values['cid'])) {
    $result = db_update('phpmailer_contact_contacts')
      ->fields($save_array)
      ->condition('cid', $form_values['cid'])
      ->execute();
  }
  else {
    $result = db_insert('phpmailer_contact_contacts')
      ->fields($save_array)
      ->execute();
  }
  if ($result == TRUE) {
    drupal_set_message(t('Settings have been updated.'));
  }
  else {
    drupal_set_message(t('There was a problem updating the settings.'), 'error');
  }
}

/**
 * Deletes contact record from database.
 *
 * @param int $cid
 *   $cid This will define which record gets deleted.
 *
 * @see phpmailer_contact_settings_form_submit()
 */
function phpmailer_contact_delete($cid) {
  $result = db_delete('phpmailer_contact_contacts')->condition('cid', $cid)->execute();
  if ($result) {
    drupal_set_message(t('Contact was successfully deleted.'));
  }
  else {
    drupal_set_message(t('Unable to delete contact.'), 'error');
  }
}

/**
 * Sends an e-mail.
 *
 * @see phpmailer_contact_form()
 */
function phpmailer_contact_mail_send($form_values) {

  $contacts = phpmailer_contact_get_contact_by_cid($form_values['contact']);
  $encrypted_password = $contacts['encrypted_password'];
  $decrypted_password = phpmailer_contact_decrypt($encrypted_password);

  $subject = $form_values['subject'] . " at " . strftime("%T", time());
  $message = $form_values['message'];
  $message = wordwrap($message, 70);
  $reply_to = $form_values['email'];
  $reply_to_name = $form_values['name'];
  $from_name = $form_values['name'];

  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->Host = $contacts['host'];
  $mail->Port = $contacts['port'];
  $mail->SMTPAuth = $contacts['auth'];
  $mail->SMTPSecure = $contacts['protocol'];
  $mail->Username = $contacts['username'];
  $mail->Password = $decrypted_password;

  $mail->AddReplyTo($reply_to, $reply_to_name);
  $mail->SetFrom($contacts['from'], $from_name);
  $mail->AddAddress($contacts['to'], $contacts['name']);
  $mail->Subject = $subject;
  $mail->Body = $message;

  $result = $mail->Send();

  if ($result == TRUE) {
    drupal_set_message(t('Your message has been sent.'));
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.  Please check your settings.'), 'error');
  }
}

/* Common querries */

/**
 * Fetches key and IV from the database.
 *
 * @return array
 *   $key Array $key['key'] and $key['iv'].
 *
 * @see phpmailer_contact_encrypt()
 * @see phpmailer_contact_decrypt()
 */
function phpmailer_contact_get_key() {
  $result = db_query('SELECT * FROM {phpmailer_contact_key}');
  if ($row = $result->fetchAssoc()) {
    $key = array(
      'key' => $row['phpmailer_contact_key_value'],
      'iv' => $row['phpmailer_contact_iv'],
    );
  }
  else {
    drupal_set_message(t('There was a problem getting the key.'), 'error');
  }
  return $key;
}

/**
 * Fetches all contacts from the database.
 *
 * @return array
 *   $result An array of all records from phpmailer_contact_contacts.
 *
 * @see phpmailer_contact_get_contacts_form()
 * @see phpmailer_contact_form()
 */
function phpmailer_contact_get_all_contacts() {
  $result = db_query('SELECT * FROM {phpmailer_contact_contacts}');
  if ($result) {
    return $result;
  }
  else {
    drupal_set_message(t('Unable to get contacts from database.'), 'error');
  }
}

/**
 * Fetches one contact record from the database based on name.
 *
 * @return array
 *   $result_set All fields from a specified contact record.
 */
function phpmailer_contact_get_contact_by_name($form_values) {
  $name = $form_values['contact'];
  $result = db_query('SELECT * FROM {phpmailer_contact_contacts} WHERE phpmailer_contact_name = :name', array(':name' => $name));
  $result_set = $result->fetchAssoc();
  return $result_set;
}

/**
 * Fetches one contact from the database based on cid.
 *
 * @param int $cid
 *   $cid Primary key for a contact record.
 *
 * @return array
 *   $contacts All fields from a specified contact record.
 *
 * @see phpmailer_contact_settings_form()
 * @see phpmailer_contact_mail_send()
 */
function phpmailer_contact_get_contact_by_cid($cid = NULL) {
  $result = db_query('SELECT * FROM {phpmailer_contact_contacts} WHERE cid = :cid', array(':cid' => $cid));
  if ($result->rowCount() >= 1) {
    while ($row = $result->fetchAssoc()) {
      $contacts = array(
        'cid' => $row['cid'],
        'name' => $row['phpmailer_contact_name'],
        'to' => $row['phpmailer_contact_to'],
        'from' => $row['phpmailer_contact_to'],
        'host' => $row['phpmailer_contact_host'],
        'port' => $row['phpmailer_contact_port'],
        'auth' => $row['phpmailer_contact_auth'],
        'protocol' => $row['phpmailer_contact_protocol'],
        'username' => $row['phpmailer_contact_username'],
        'encrypted_password' => $row['phpmailer_contact_password'],
      );
    }
  }
  else {
    $contacts = NULL;
  }
  return $contacts;
}
